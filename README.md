# Traefik 

|  Variable   |      Valor por defecto      |                       Links                       |
|:-----------:|:---------------------------:|:-------------------------------------------------:|
| TRAEFIK_TAG |            v2.6             | [Tags](https://hub.docker.com/_/traefik?tab=tags) |
| TRAEFIK_URL | traefik-local.midominio.com |                         -                         |


## TRAEFIK_TAG

Version del traefik, en el momento de esta documentación se probó con exito la version v2.6 por lo que no se 
garantiza que funcione con diferente version.  

- [versiones](https://hub.docker.com/_/traefik?tab=tags)
- [documentacion](https://doc.traefik.io/traefik/)


## TRAEFIK_URL

Url donde se va a exponer la pagina web del dashboard de traefik

